package com.linenotify;

import com.linenotify.component.NotifyComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class LinenotifyApplication {

    @Autowired
    NotifyComponent notifyComponent;

    public static void main(String[] args) {
        SpringApplication.run(LinenotifyApplication.class, args);
    }

    @Bean
    CommandLineRunner init() {
        return args -> {
            notifyComponent.executeNotification();
        };
    }

}
