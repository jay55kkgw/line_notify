package com.linenotify.component;

import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;

@Component
public class NotifyComponent {

    private final URI lineUrl = URI.create("https://notify-api.line.me/api/notify");
    private final String contentType = "application/x-www-form-urlencoded";

    public void executeNotification() {
        String message = "Message you want to send";
        String token = "w6pEMsTX0LBpcNH89CScNNT9oCyTth2bNrh5BnPXKsW";

        sendNotification(message,token);
    }

    public void sendNotification(String message, String token) {
        HttpURLConnection connection = null;
        try {
            URL url = new URL("https://notify-api.line.me/api/notify");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.addRequestProperty("Authorization", "Bearer " + token);
            try (OutputStream outputStream = connection.getOutputStream();
                 PrintWriter writer = new PrintWriter(outputStream)) {
                writer.append("message=").append(URLEncoder.encode(message, "UTF-8")).flush();
            }
            System.out.println(connection.getResponseCode());
        } catch (Exception e) {
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

//        String bearerAuth = "Bearer " + token;
//        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
//        headers.add("Content-Type", contentType);
//        headers.add("Content-Length", "" + message.getBytes().length);
//        headers.add("Authorization", bearerAuth);
//        RequestEntity request = new RequestEntity(
//                "message="+message, headers, HttpMethod.POST, lineUrl);
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity responseEntity = restTemplate.exchange(request, String.class);
    }
}
